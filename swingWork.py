addLibPath("/Users/jchurchill2016/Documents/Python_Basics/final-project/")
from javax.swing import *
from java.awt.event import *
from java.awt import *
from java.awt.BorderLayout import *
from freshStart import *
def react(event):
  message.text = "You Pressed Button One"
  panel.setBackground(Color(67,89,234))

def reactTwo(event):
  message.text = "You Pressed Button Two"
  
frame = JFrame("Button Work", size = (300,160))  
panel = JPanel()
panel.setLayout(None)
panel.setBackground(Color(66, 66, 66))
panel.setSize(100,100)
frame.add(panel)

button = JButton("ASCII", actionPerformed = ASCII)
button.setBounds(25, 25, 100, 50)

buttonTwo = JButton("Button Two", actionPerformed = reactTwo)
buttonTwo.setBounds(150, 25, 100, 50)

panel.add(button)
panel.add(buttonTwo)
message = JLabel(" ")
frame.add(message, NORTH)

frame.visible = True